import { Suspense, useMemo } from "react";
import { CssBaseline, CircularProgress } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import GameWrapper from "../gameWrapper";
import "./App.css";

function App() {
  const theme = useMemo(() => createTheme(), []);

  return (
    <Suspense fallback={<CircularProgress />}>
      <CssBaseline />
      <ThemeProvider theme={theme}>
        <div className="App">
          <GameWrapper />
        </div>
      </ThemeProvider>
    </Suspense>
  );
}

export default App;
