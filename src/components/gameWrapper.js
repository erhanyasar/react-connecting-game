import { useState } from "react";
import {
  Typography,
  Container,
  Box,
  Grid,
  Stack,
  Alert,
  Badge,
  IconButton,
} from "@mui/material";
import RefreshIcon from "@mui/icons-material/Refresh";

function GameWrapper() {
  const [hasError, setHasError] = useState(false);
  const [isConnected, setIsConnected] = useState([
    { initialState: true, selected: false, choosen: false },
    { initialState: true, selected: false, choosen: false },
    { initialState: true, selected: false, choosen: false },
    { initialState: true, selected: false, choosen: false },
    { initialState: true, selected: false, choosen: false },
  ]);
  const [showAnswers, setShowAnswers] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);
  const [questionAndAnswers, setQuestionAndAnswers] = useState([]);
  const [allQuestionsAnswered, setAllQuestionsAnswered] = useState(false);
  const [animalsTranslated, setAnimalsTranslated] = useState([
    {
      name: "Horse",
      translation: "حصان",
      isCorrect: false,
    },
    { name: "Dog", translation: "كلب", isCorrect: false },
    { name: "Cow", translation: "بقرة", isCorrect: false },
    { name: "Duck", translation: "بطة", isCorrect: false },
    { name: "Cat", translation: "قطة", isCorrect: false },
  ]);

  const clickVoice = new Audio("./assets/audio/classic-click.wav");
  const pluggedVoice = new Audio("./assets/audio/plugged.wav");
  const submitButtonVoice = new Audio("./assets/audio/submit-answers.wav");
  const dogBarking = new Audio("./assets/audio/dog-barking.wav");

  const selectOption = (i) => {
    if (hasError) setHasError(false);
    setSelectedOption(i);
    setIsConnected((prevState) => {
      prevState[i] = { ...prevState[i], selected: true };
      return [...prevState];
    });
    clickVoice.play();
  };

  const chooseAnswer = (i) => {
    if (selectedOption !== null) {
      setIsConnected((prevState) => {
        if (selectedOption !== i) {
          prevState[selectedOption] = {
            ...prevState[selectedOption],
            selected: selectedOption,
            choosen: i,
            initialState: false,
          };
          prevState[i] = {
            ...prevState[i],
            isChoosen: true,
          };
        } else
          prevState[i] = {
            ...prevState[i],
            selected: selectedOption,
            choosen: i,
            initialState: false,
          };
        return [...prevState];
      });
      pluggedVoice.play();
      setQuestionAndAnswers((prevState) => {
        if (!questionAndAnswers.length) {
          return [
            ...prevState,
            {
              key: animalsTranslated[selectedOption].name,
              value: animalsTranslated[i].translation,
            },
          ];
        }

        let tempState;

        prevState?.forEach((state, index) => {
          if (
            state.key !== animalsTranslated[selectedOption].name &&
            animalsTranslated[i].translation !== state.value
          ) {
            tempState = [
              ...prevState,
              {
                key: animalsTranslated[selectedOption].name,
                value: animalsTranslated[i].translation,
              },
            ];
          } else if (animalsTranslated[i].translation === state.value) {
            prevState?.forEach((element, idx) => {
              if (animalsTranslated[i].translation === element.value) {
                tempState = prevState.toSpliced(i, 1);

                tempState = [
                  ...tempState,
                  {
                    key: animalsTranslated[selectedOption].name,
                    value: animalsTranslated[i].translation,
                  },
                ];
              }
            });
          } else {
            prevState[index].value = animalsTranslated[i].translation;
            tempState = prevState;
          }
        });

        return tempState;
      });

      setSelectedOption(null);
    } else setHasError(true);
    checkAllQuestionsAnswered();
  };

  const checkAllQuestionsAnswered = () => {
    if (questionAndAnswers?.length === 4) setAllQuestionsAnswered(true);
  };

  const submitAnswers = () => {
    submitButtonVoice.play();
    if (allQuestionsAnswered) validateAnswers();
  };

  const validateAnswers = () => {
    setAnimalsTranslated((prevState) => {
      animalsTranslated.forEach((animal) => {
        questionAndAnswers.forEach((qa, index) => {
          if (qa.key === animal.name && qa.value === animal.translation)
            animal.isCorrect = true;
        });
      });
      return [...prevState];
    });
    setShowAnswers(true);
  };

  const refreshGame = () => {
    setQuestionAndAnswers([]);
    setSelectedOption(null);
    setShowAnswers(false);
    setIsConnected([
      { initialState: true, values: [null, null] },
      { initialState: true, values: [null, null] },
      { initialState: true, values: [null, null] },
      { initialState: true, values: [null, null] },
      { initialState: true, values: [null, null] },
    ]);
    setAllQuestionsAnswered(false);
  };

  return (
    <Container maxWidth="sm">
      <Box sx={{ flexGrow: 1 }}>
        <Stack
          direction="row"
          sx={{ display: "flex", justifyContent: "space-between" }}
        >
          <Grid container>
            {animalsTranslated.map((animal, index) => {
              return (
                <Grid
                  item
                  xs={8}
                  key={index}
                  onClick={() => selectOption(index)}
                >
                  <Stack
                    direction="row"
                    sx={{ display: "flex" }}
                    alignItems="center"
                  >
                    <img
                      src="./assets/img/Plug_base.png"
                      alt="plug base"
                      width="225px"
                      height="75px"
                    />
                    {(isConnected[index]?.initialState ||
                      isConnected[index]?.selected === true ||
                      (isConnected[index]?.selected === index &&
                        !isConnected[index]?.choosen === index)) && (
                      <img
                        src="./assets/img/plug.png"
                        alt="plug"
                        width="30px"
                        style={{
                          marginLeft: "-37px",
                          marginTop: "15px",
                          transform: "rotateY(0deg) rotate(60deg)",
                          transition: "transform 2s",
                        }}
                      />
                    )}
                  </Stack>
                  <Typography
                    variant="h5"
                    component="h1"
                    gutterBottom
                    sx={{
                      marginTop: "-50px",
                      marginLeft: "25px",
                      fontWeight: "bolder",
                      color: "white",
                    }}
                  >
                    {animal.name}
                  </Typography>
                </Grid>
              );
            })}
          </Grid>
          <Grid container>
            {animalsTranslated.map((animal, index) => {
              return (
                <Grid
                  item
                  xs={8}
                  key={index}
                  onClick={() => chooseAnswer(index)}
                >
                  <Stack
                    direction="row"
                    sx={{ display: "flex" }}
                    alignItems="center"
                  >
                    {((isConnected[index]?.selected === index &&
                      isConnected[index]?.choosen === index) ||
                      isConnected[index]?.isChoosen) && (
                      <img
                        src="./assets/img/plug.png"
                        alt="plug"
                        width="30px"
                        style={{
                          marginTop: "15px",
                          transform: "rotateY(0deg) rotate(-70deg)",
                          transition: "transform 2s",
                          position: "absolute",
                          zIndex: 2,
                        }}
                      />
                    )}
                    <img
                      src="./assets/img/PlugF_base_circle.png"
                      alt="plug F base circle"
                      width="40px"
                      height="40px"
                      style={{ zIndex: 1 }}
                    />
                    <img
                      src="./assets/img/PlugF_base.png"
                      alt="plug F base"
                      width="225px"
                      height="75px"
                      style={{ marginLeft: "-40px" }}
                    />
                    <img
                      width="25px"
                      height="25px"
                      src="./assets/img/light_grey.png"
                      style={{ marginLeft: "-40px" }}
                      alt="grey light"
                    />
                    {showAnswers && (
                      <img
                        width="15px"
                        height="15px"
                        src={
                          animal.isCorrect !== false
                            ? "./assets/img/light_green.png"
                            : "./assets/img/light_red.png"
                        }
                        style={{ marginLeft: "-20px" }}
                        alt="green or red light"
                      />
                    )}
                    <img
                      width="30px"
                      height="30px"
                      style={{ marginLeft: "10px" }}
                      src="./assets/img/sound.png"
                      alt="sound"
                      onClick={() => dogBarking.play()}
                    />
                  </Stack>
                  <Typography
                    variant="h5"
                    component="h1"
                    gutterBottom
                    sx={{
                      marginTop: "-50px",
                      marginLeft: "50px",
                      fontWeight: "bolder",
                      color: "white",
                    }}
                  >
                    {animal.translation}
                  </Typography>
                  {showAnswers && animal.isCorrect === true && (
                    <Badge
                      badgeContent={5}
                      color="success"
                      sx={{
                        position: "absolute",
                        marginLeft: "175px",
                        marginTop: "-30px",
                      }}
                    />
                  )}
                </Grid>
              );
            })}
          </Grid>
        </Stack>
        {allQuestionsAnswered && (
          <Stack direction="row" sx={{ justifyContent: "center" }}>
            <img
              src="./assets/img/arrow.png"
              alt="arrow button"
              width="75px"
              height="75px"
              style={{ marginRight: "50px" }}
              onClick={submitAnswers}
            />
            <IconButton aria-label="delete" onClick={refreshGame}>
              <RefreshIcon fontSize="large" color="primary" />
            </IconButton>
          </Stack>
        )}
        {hasError && (
          <Stack sx={{ width: "100%", mt: 5 }}>
            <Alert variant="filled" severity="warning">
              First, choose an item on the left!
            </Alert>
          </Stack>
        )}
      </Box>
    </Container>
  );
}

export default GameWrapper;
